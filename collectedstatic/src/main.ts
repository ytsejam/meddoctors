import { createApp } from 'vue'
import App from './App.vue'
import './index.css'
import router from './router/index.js';
import * as apolloProvider from './apollo-provider.js' 
createApp(App).use(router).use(apolloProvider.provider).mount('#app');