"""medtourism URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles import views as stat
#from django.contrib import admin
from baton.autodiscover import admin
from django.views.generic import TemplateView
from django.views.decorators.csrf import csrf_exempt
from graphene_django.views import GraphQLView
from django.views.static import serve  
from django.urls import path, re_path, include
from django.contrib.auth import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from medtourism.apps.profiles.urls import router
from medtourism.apps.core.views import frontpage, signup
from medtourism.apps.sendemails.views import contactView
urlpatterns = [
    path('admin/', admin.site.urls),
    path('baton/', include('baton.urls')),
    path('homebackup', frontpage, name='frontpage'),
    path('api/', include(router.urls)),
    path('signup/', signup, name='signup'),
    path('send_email/', contactView, name='sendemail'),
    path('login/', views.LoginView.as_view(template_name = 'core/login.html'), name='login'),
    path('logout/', views.LogoutView.as_view(), name='logout'),
    path('api-auth/',include('rest_framework.urls', namespace='rest_framework')),
    path('graphql/', csrf_exempt(GraphQLView.as_view(graphiql=True))),
    path('api/v1/', include('medtourism.apps.categories.urls')),
    path('api/v1/', include('medtourism.apps.pages.urls')),
    path('api/v1/', include('medtourism.apps.posts.urls')),
    path('api/v1/', include('medtourism.apps.treatments.urls')),
    path('api/v1/', include('medtourism.apps.tags.urls')),
    path('api/v1/send_email/', contactView, name='sendemail'),
    re_path(r'^media/(?P<path>.*)$', serve, {
        'document_root': settings.MEDIA_ROOT
    }),
    re_path("",
         TemplateView.as_view(template_name="index.html"),
         name="app",
         ),
]

if settings.DEBUG:     
    urlpatterns += staticfiles_urlpatterns()  
    urlpatterns += static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)