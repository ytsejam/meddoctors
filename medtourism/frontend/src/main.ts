import {createApp} from 'vue';
import App from './App.vue';
import router from './router';
import * as apolloProvider from './apollo-provider' 
createApp(App).use(router).use(apolloProvider.provider).mount('#app');