const path = require('path');
const { VueLoaderPlugin } = require('vue-loader');
const BundleTracker = require('webpack-bundle-tracker');
const HtmlWebpackPlugin = require('html-webpack-plugin');

 module.exports = {
   mode:'production',
   entry:{
    app: path.resolve(__dirname, './src/main.ts'),
   },
   output: {
     filename: '[name]-[fullhash].js',
     path: path.resolve(__dirname, './assets/dist'),
     clean: true,
     publicPath:'http://0.0.0.0:8080',
   },
   module: {
      rules: [
        {
          test: /\.vue$/,
          use: 'vue-loader'
        },
        {
          test: /\.ts$/,
          loader: 'ts-loader',
          options: {
            appendTsSuffixTo: [/\.vue$/],
          }
        },
        {
          test: /\.css$/i,
          use: [ "style-loader", "css-loader"],
        },
        {
          test: /\.(png|jpe?g|gif|svg|eot|ttf|woff|woff2)$/i,
          // More information here https://webpack.js.org/guides/asset-modules/
          type: "asset",
        },
      ],
    },
    resolve: {
      extensions: ['.ts', '.js', '.vue', '.json'],
      alias: {
        'vue': '@vue/runtime-dom',
        'bulma': 'bulma/css/bulma.css',
      }
    },
   plugins: [
     new VueLoaderPlugin(),
     new BundleTracker({
        filename: './webpack-stats.json',
	      publicPath: '/'
      })
   ]
 };
