 const { merge } = require('webpack-merge');
 const common = require('./webpack.common.js');
 const path = require('path');

 module.exports = merge(common, {
   mode: 'production',
   output: {
     filename: '[name]-[fullhash].js',
     path: path.resolve(__dirname, './assets/dist'),
     clean: true,
     publicPath:'https://medtourism.uk/static/',
   },
   devtool: 'source-map'
 });