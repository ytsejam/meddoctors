module.exports = {
  purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend:  {
      fontFamily: {
      'body': ['Open Sans', 'sans-serif'],
      'title': ['Roboto', 'sans-serif']
      },
      colors: {
        'pri':  '#081F4D'
      }
    }, 
  },
  variants: {
      extend: {},
  },
  plugins: [
    require('@tailwindcss/forms'),
    require('@tailwindcss/typography'),
  ],
}