import { createApp } from 'vue'
import App from './App.vue'
import './index.css';
import '/src/assets/css/custom.css'
import '/src/assets/css/style.css'
import Oruga from '@oruga-ui/oruga-next'
import '@oruga-ui/oruga-next/dist/oruga.css'
import router from './router/index.js';
import * as apolloProvider from './apollo-provider.js' 
createApp(App).use(Oruga).use(router).use(apolloProvider.provider).mount('#app');