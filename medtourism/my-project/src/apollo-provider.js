import { createApolloProvider } from '@vue/apollo-option';
import { ApolloClient } from 'apollo-client'
import { createHttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'
// HTTP connection to the API
const httpLink = createHttpLink({
  // You should use an absolute URL here
  //uri: 'http://localhost:8000/graphql/',
  uri: 'http://localhost:8000/graphql/',
})

// Cache implementation
const cache = new InMemoryCache()

// Create the apollo client
const apolloClient = new ApolloClient({
  link: httpLink,
  cache,
})
export const provider = createApolloProvider({
  defaultClient: apolloClient,
});