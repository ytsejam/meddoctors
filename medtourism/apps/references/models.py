from django.contrib.auth.models import User
from django.db import models

class Reference(models.Model):
    user = models.ForeignKey(User, related_name="references", on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    show_description = models.TextField()
    description = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title