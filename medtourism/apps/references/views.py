from django.shortcuts import render
from rest_framework import viewsets
from .serializers import ReferenceSerializer
from .models import Reference

from django.core.exceptions import PermissionDenied

class ReferencesViewSet(viewsets.ModelViewSet):
	serializer_class = ReferenceSerializer
	queryset = Reference.objects.all()

	def get_queryset(self):
		return self.queryset.all()

	def perform_create(self, serializer):
		serializer.save()

	def perform_update(self, serializer):
		obj = self.get_object()

		if self.request.user != obj.created_by:
			raise PermissionDenied('Wrong object owner')

		serializer.save()