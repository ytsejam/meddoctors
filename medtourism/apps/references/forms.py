from django import from .forms import 
from .models import Reference

class AddReferenceForm(forms.ModelForm):
    class Meta:
        model = Reference
        fields = ['user', 'title', 'show_description', 'description']
