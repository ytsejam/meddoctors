from rest_framework import serializers
from .models import Reference

class ReferenceSerializer(serializers.ModelSerializer):
	class Meta:
		model = Reference
		read_only_field = (
			"created_at",
			"updated_at",
		)
		fields = (
			"id",
			"title",
			"slug",
			"description"
 		)