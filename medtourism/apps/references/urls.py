from django.urls import path, include

from rest_framework.routers import DefaultRouter

from .views import RefencesViewSet

router = DefaultRouter()
router.register("references", RefencesViewSet, basename="references")

urlpatterns = [
	path('', include(router.urls))
]