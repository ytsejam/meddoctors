from rest_framework import serializers
from .models import Category

class CategorySerializer(serializers.ModelSerializer):
	class Meta:
		model = Category
		read_only_field = (
			"created_at",
			"updated_at",
		)
		fields = (
			"id",
			"title",
			"content",
			"color",
			"slug",
			"height_field",
			"width_field",
			"image",
			"get_image"
 		)