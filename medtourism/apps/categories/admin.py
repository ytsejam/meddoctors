from django.contrib import admin
from django import forms
from medtourism.apps.categories.models import Category
from medtourism.apps.categories.forms import CategoryModelForm

# Register your models here.


class CategoryAdmin(admin.ModelAdmin):
  form = CategoryModelForm
  prepopulated_fields = {'slug': ('title',), }
  class Meta:
    model = Category
    
admin.site.register(Category, CategoryAdmin)
