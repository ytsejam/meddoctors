from django import forms
from django.contrib import admin
from django.db import models
from django.forms import Textarea, ModelForm
from django.forms.widgets import TextInput
from .models import Category

 

class CategoryModelForm(forms.ModelForm):

    class Meta:
       model=Category
       fields = [ "title", "slug", "color","image"]
       widgets = {
            'color': TextInput(attrs={'type': 'color'}),
        }
