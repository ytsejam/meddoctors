# Generated by Django 3.2.6 on 2021-12-01 11:56

import ckeditor.fields
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import medtourism.apps.posts.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('tags', '0001_initial'),
        ('categories', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255)),
                ('slug', models.SlugField(unique=True)),
                ('excerpt', ckeditor.fields.RichTextField()),
                ('content', ckeditor.fields.RichTextField()),
                ('height_field', models.IntegerField(default=0)),
                ('width_field', models.IntegerField(default=0)),
                ('draft', models.BooleanField(default=False)),
                ('filter_order', models.PositiveIntegerField(default=1)),
                ('image', models.ImageField(blank=True, height_field='height_field', null=True, upload_to=medtourism.apps.posts.models.Post.upload_location, width_field='width_field')),
                ('publish', models.DateField()),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('categories', models.ManyToManyField(default=[1], related_name='category_posts', to='categories.Category', verbose_name='related_categories')),
                ('tags', models.ManyToManyField(default=[1], to='tags.Tag', verbose_name='related_categories')),
                ('user', models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='articles', to=settings.AUTH_USER_MODEL, verbose_name='related_user')),
            ],
            options={
                'verbose_name': 'Post',
                'verbose_name_plural': 'Posts',
                'ordering': ('title',),
                'unique_together': {('title',)},
            },
        ),
    ]
