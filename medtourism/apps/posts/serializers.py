from rest_framework import serializers
from .models import Post

class PostsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        read_only_field = (
            "created_at",
            "updated_at",
        )
        fields = (
            "id",
            "title",
            "slug",
            "content"
        )

