from django.shortcuts import render
from rest_framework import viewsets
from .serializers import PostsSerializer
from .models import Post

from django.core.exceptions import PermissionDenied

class PostsViewSet(viewsets.ModelViewSet):
	serializer_class = PostsSerializer
	queryset = Post.objects.all()

	def get_queryset(self):
		return self.queryset.all()

	def perform_create(self, serializer):
		serializer.save()

	def perform_update(self, serializer):
		obj = self.get_object()

		if self.request.user != obj.created_by:
			raise PermissionDenied('Wrong object owner')

		serializer.save()