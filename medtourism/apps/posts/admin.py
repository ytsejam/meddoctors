from django.contrib import admin
from django import forms
from medtourism.apps.posts.forms import PostModelForm

from .models import Post
from ckeditor.widgets import CKEditorWidget
 
 
class PostAdmin(admin.ModelAdmin):
	content = forms.CharField(widget=CKEditorWidget())
	form = PostModelForm
	prepopulated_fields = {'slug': ('title',), }
	class Meta:
		model = Post


list_display = (
        "id",
        "title",
        "slug",
        "created_at",
        "publish",
    )
list_filter = (
    "publish",
    "created_at",
)
list_editable = (
    "title",
    "slug",
    "created_at",
    "publish",
)
search_fields = (
    "title",
    "slug",
    "content",
)
prepopulated_fields = {
    "slug": (
        "title",
    )
}
date_hierarchy = "created_at"
save_on_top = True