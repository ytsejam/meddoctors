from django import forms
#from ckeditor_uploader.widgets import CKEditorUploadingWidget
from .models import Post



class PostModelForm(forms.ModelForm):
	#content = forms.CharField(widget=CKEditorUploadingWidget)
	class Meta:
	   model=Post
	   fields = [ 
	   				"title", 
	   				"slug",
	   				"filter_order", 
	   				"user", 
	   				"categories", 
	   				"tags", 
	   				"content",
	   				"excerpt",
	   				"image",
	   				"draft",
	   				"publish",]
