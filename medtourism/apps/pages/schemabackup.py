from graphene_django import DjangoObjectType
import graphene
from .models import Page
class PageType(DjangoObjectType):
    class Meta:
        model = Page
        fields = ("id", "title", "slug", "content")

class Query(graphene.ObjectType):
    pages = graphene.List(PageType)

    def resolve_pages(self, info):
        return Page.objects.all()

class PageMutation(graphene.Mutation):
    page = graphene.Field(PageType)
    class Arguments:
        id = graphene.ID()
        title = graphene.String(required=True)
        slug = graphene.String(required=True)
        content = graphene.String()

    @classmethod
    def mutate(cls, root, title, slug, content):
        page = Page.objects.get(slug=slug)
        page.title = title
        page.content = content
        page.save()
        return PageMutation(page=page)

class Mutation(PageMutation, graphene.Mutation):
    pass

schema = graphene.Schema(query=Query, mutation=Mutation)