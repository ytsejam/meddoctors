from rest_framework import serializers
from .models import Page

class PagesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Page
        read_only_field = (
            "created_at",
            "updated_at",
        )
        fields = (
            "id",
            "title",
            "slug",
            "content"
        )

