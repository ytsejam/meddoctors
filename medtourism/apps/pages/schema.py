import graphene
from graphene import Argument
from graphene_django.types import DjangoObjectType
from .models import Page

class PageType(DjangoObjectType):
	class Meta:
		model = Page

class Query(graphene.ObjectType):
	all_pages = graphene.List(PageType)
	page_by_slug = graphene.Field(PageType, slug=graphene.String())
	page = graphene.Field(PageType, id=graphene.ID())

	def resolve_all_pages(self, info, **kwargs):
		return Page.objects.all()

	def resolve_page_by_slug(self, info, slug=None):
		return Page.objects.get(slug=slug)

class CreatePage(graphene.Mutation):
	class Arguments:
		title = graphene.String()
		slug = graphene.String()
		content = graphene.String()
		created_at = graphene.types.datetime.DateTime()
		updated_at = graphene.types.datetime.DateTime()

	def mutate(self, info, title, content=None, date_created=None):
	    page = Page.objects.create(
	      title = title,
	      slug = slug,
	      content = content,
	      created_at = created_at,
	      updated_at = updated_at,
	    )
	    page.save()
	    return CreatePage(
			page = page
		)

class UpdatePage(graphene.Mutation):
	class Arguments:
		id = graphene.ID()
		title = graphene.String()
		slug = graphene.String()
		content = graphene.String()
		created_at = graphene.types.datetime.DateTime()
		updated_at = graphene.types.datetime.DateTime()

	page = graphene.Field(PageType)

	def mutate(self, info, title, content=None, date_created=None):
		page = Page.objects.get(pk=id)
		page.title = title if title is not None else page.title
		page.slug = slug if slug is not None else page.slug
		page.content = content if content is not None else page.content
		page.created_at = created_at if created_at is not None else page.created_at
		page.updated_at = updated_at if updated_at is not None else page.updated_at
		page.save()

class DeletePage(graphene.Mutation):
	class Arguments:
		id = graphene.ID()
	
	page = graphene.Field(PageType)

	def mutate(self, info, id):
		page = Page.objects.get(pk=id)
		if page is not None:
			page.delete()
		return DeleteProduct(page=page)

class Mutation(graphene.ObjectType):
  create_page = CreatePage.Field()
  update_page = UpdatePage.Field()
  delete_page = DeletePage.Field()

schema = graphene.Schema(query=Query, mutation=Mutation)