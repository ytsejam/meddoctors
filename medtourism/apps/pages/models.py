from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _
from django.utils.text import slugify

from ckeditor.fields import RichTextField

class Page(models.Model):
    title = models.CharField(max_length=255)
    slug = models.SlugField(unique=True)
    content = RichTextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def create_slug(instance, new_slug=None):
        slug = slugify(instance.title)
        if new_slug is not None:
            slug = new_slug
        qs = Page.objects.filter(slug=slug).order_by("-id")
        exists = qs.exists()
        if exists:
            new_slug = "%s-%s" % (slug, qs.first().id)
            return create_slug(instance, new_slug=new_slug)
        return slug

    class Meta:
        verbose_name = _("Page")
        verbose_name_plural = _("Pages")
        ordering = ( "title",)
        unique_together = ( "title",)

    def __str__(self):
        return "{}".format( self.title)

@receiver(pre_save, sender=Page)
def pre_save_page_receiver(sender, instance, raw, using, **kwargs):
    if instance and not instance.slug:
        slug = slugify(instance.title)
        random_string = generate_random_string()
        instance.slug = slug + "_" + random_string

pre_save.connect(pre_save_page_receiver, sender=Page)