from django.shortcuts import render
from rest_framework import viewsets
from .serializers import PagesSerializer
from .models import Page

from django.core.exceptions import PermissionDenied

class PagesViewSet(viewsets.ModelViewSet):
	serializer_class = PagesSerializer
	queryset = Page.objects.all()
	lookup_field = 'slug'
	##does not work
	#def get_queryset(self, slug=None):
	#	return self.queryset.get(slug=slug)
	#def get_queryset(self):
	#	return self.queryset.all()

	def perform_create(self, serializer):
		serializer.save()

	def perform_update(self, serializer):
		obj = self.get_object()

		if self.request.user != obj.created_by:
			raise PermissionDenied('Wrong object owner')

		serializer.save()