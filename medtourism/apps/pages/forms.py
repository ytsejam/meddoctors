from django import forms
#from ckeditor_uploader.widgets import CKEditorUploadingWidget
from .models import Page

class PageModelForm(forms.ModelForm):
	#content = forms.CharField(widget=CKEditorUploadingWidget)
	class Meta:
	   model=Page
	   fields = [ 
	   				"title", 
	   				"slug",
	   				"content",
	   				]
