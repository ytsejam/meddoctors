from django import forms
from django.contrib import admin
from .forms import PageModelForm
from .models import Page
from ckeditor.widgets import CKEditorWidget


class PageAdmin(admin.ModelAdmin):
  content = forms.CharField(widget=CKEditorWidget())
  form = PageModelForm
  prepopulated_fields = {'slug': ('title',), }
  class Meta:
    model = Page
    
admin.site.register(Page, PageAdmin)

 