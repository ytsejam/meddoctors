from django.urls import path, include

from rest_framework.routers import DefaultRouter

from .views import PagesViewSet

router = DefaultRouter()
router.register("pages", PagesViewSet, basename="pages")

urlpatterns = [
	path('', include(router.urls))
]