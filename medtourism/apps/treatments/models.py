from django.conf import settings
from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _
from django.utils.text import slugify
from sorl.thumbnail import ImageField
from medtourism.apps.categories.models import Category
from medtourism.apps.tags.models import Tag
from ckeditor.fields import RichTextField

class Treatment(models.Model):
    def upload_location(instance, filename):
        #filebase, extension = filename.split(".")
        # return "%s/%s.%s" %(instance.id, instance.id, extension)
        try:
            TreatmentModel = instance.__class__
            new_id = TreatmentModel.objects.order_by("id").last().id + 1
        except:
            new_id=1

        return "{} {}".format(new_id, filename)

    title = models.CharField(max_length=255)
    # Relations
    categories = models.ManyToManyField(Category,  default=[1],  verbose_name=_(
        "related_categories"), related_name="category_treatments")
    tags = models.ManyToManyField(Tag,  default=[1],  verbose_name=_(
        "related_tags"), related_name="tag_treatments" )
    user = models.ForeignKey(settings.AUTH_USER_MODEL, default=1,  verbose_name=_(
        "related_user"),related_name="treatments", on_delete=models.CASCADE,)
    slug = models.SlugField(unique=True)
    height_field = models.IntegerField(default=0)
    width_field = models.IntegerField(default=0)
    image = models.ImageField(
                            null=True,
                            blank=True,
                            upload_to=upload_location,
                            height_field="height_field",
                            width_field="width_field")
    excerpt = RichTextField()
    content = RichTextField()
    draft = models.BooleanField()
    filter_order = models.PositiveIntegerField(default=1)
    publish = models.DateField(auto_now=False, auto_now_add=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def create_slug(instance, new_slug=None):
        slug = slugify(instance.title)
        if new_slug is not None:
            slug = new_slug
        qs = Post.objects.filter(slug=slug).order_by("-id")
        exists = qs.exists()
        if exists:
            new_slug = "%s-%s" % (slug, qs.first().id)
            return create_slug(instance, new_slug=new_slug)

        return slug

    class Meta:
        verbose_name = _("Treatment")
        verbose_name_plural = _("Treatments")
        ordering = ( "title",)
        unique_together = ( "title",)

    def __str__(self):
      return "{}".format( self.title)

class Treatmentsubs(models.Model):
    treatment = models.ForeignKey(Treatment, related_name="titles", on_delete=models.CASCADE)
    title = models.CharField(max_length=240)
    slug = models.SlugField(unique=True)
    content = RichTextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def create_slug(instance, new_slug=None):
        slug = slugify(instance.title)
        if new_slug is not None:
            slug = new_slug
        qs = Post.objects.filter(slug=slug).order_by("-id")
        exists = qs.exists()
        if exists:
            new_slug = "%s-%s" % (slug, qs.first().id)
            return create_slug(instance, new_slug=new_slug)

        return slug


@receiver(pre_save, sender=Treatment)
def pre_save_treatment_receiver(sender, instance, raw, using, **kwargs):
    if instance and not instance.slug:
        slug = slugify(instance.title)
        random_string = generate_random_string()
        instance.slug = slug + "_" + random_string

pre_save.connect(pre_save_treatment_receiver, sender=Treatment)

@receiver(pre_save, sender=Treatmentsubs)
def pre_save_treatmentsubs_receiver(sender, instance, raw, using, **kwargs):
    if instance and not instance.slug:
        slug = slugify(instance.title)
        random_string = generate_random_string()
        instance.slug = slug + "_" + random_string

pre_save.connect(pre_save_treatmentsubs_receiver, sender=Treatmentsubs)