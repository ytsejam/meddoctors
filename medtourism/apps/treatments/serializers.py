from rest_framework import serializers
from .models import Treatment, Treatmentsubs

class TreatmentSerializer(serializers.ModelSerializer):
	class Meta:
		model = Treatment
		read_only_field = (
			"created_at",
			"updated_at",
		)
		fields = (
			"id",
			"title",
			"slug",
			"image",
			"content",
			"excerpt"
 		)


class TreatmentsubsSerializer(serializers.ModelSerializer):
	class Meta:
		model = Treatmentsubs
		read_only_field = (
			"created_at",
			"updated_at",
		)
		fields = (
			"id",
			"title",
			"slug",
			"content",
 		)