from django.contrib import admin
from django import forms
from .forms import TreatmentModelForm, TreatmentsubsModelForm
from .models import Treatment, Treatmentsubs
from ckeditor.widgets import CKEditorWidget
 
 
class TreatmentAdmin(admin.ModelAdmin):
	content = forms.CharField(widget=CKEditorWidget())
	form = TreatmentModelForm
	prepopulated_fields = {'slug': ('title',), }
	class Meta:
		model = Treatment
admin.site.register(Treatment, TreatmentAdmin)

class TreatmentsubsAdmin(admin.ModelAdmin):
	content = forms.CharField(widget=CKEditorWidget())
	form = TreatmentsubsModelForm
	prepopulated_fields = {'slug': ('title',), }
	class Meta:
		model = Treatmentsubs
admin.site.register(Treatmentsubs, TreatmentsubsAdmin)
