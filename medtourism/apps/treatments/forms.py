from django import forms
#from ckeditor_uploader.widgets import CKEditorUploadingWidget
from .models import Treatment, Treatmentsubs

class TreatmentModelForm(forms.ModelForm):
	#content = forms.CharField(widget=CKEditorUploadingWidget)
	class Meta:
	   model=Treatment
	   fields = [ 
	   				"title", 
	   				"slug",
	   				"filter_order", 
	   				"user", 
	   				"categories", 
	   				"tags", 
	   				"content",
	   				"excerpt",
	   				"image",
	   				"draft",
	   				"publish",]

class TreatmentsubsModelForm(forms.ModelForm):
	#content = forms.CharField(widget=CKEditorUploadingWidget)
	class Meta:
	   model=Treatmentsubs
	   fields = [ 
	   				"treatment",
	   				"title", 
	   				"slug",
	   				"content",
	   				]
