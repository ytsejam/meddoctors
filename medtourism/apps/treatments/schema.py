import graphene
from graphene import Argument
from graphene_django.types import DjangoObjectType
from .models import Treatment

class TreatmentType(DjangoObjectType):
	class Meta:
		model = Treatment

class Query(graphene.ObjectType):
	all_treatments = graphene.List(TreatmentType)
	treatment_by_slug = graphene.Field(TreatmentType, slug=graphene.String())
	treatment = graphene.Field(TreatmentType, id=graphene.ID())
	def resolve_all_treatments(self, info, **kwargs):
		return Treatment.objects.all()

	def resolve_treatment_by_slug(self, info, slug=None):
		return Treatment.objects.get(slug=slug)

class CreateTreatment(graphene.Mutation):
	class Arguments:
		title = graphene.String()
		slug = graphene.String()
		content = graphene.String()
		created_at = graphene.types.datetime.DateTime()
		updated_at = graphene.types.datetime.DateTime()

	def mutate(self, info, title, content=None, date_created=None):
	    page = Treatment.objects.create(
	      title = title,
	      slug = slug,
	      content = content,
	      created_at = created_at,
	      updated_at = updated_at,
	    )
	    treatment.save()
	    return CreateTreatment(
			treatment = treatment
		)

class UpdateTreatment(graphene.Mutation):
	class Arguments:
		id = graphene.ID()
		title = graphene.String()
		slug = graphene.String()
		content = graphene.String()
		created_at = graphene.types.datetime.DateTime()
		updated_at = graphene.types.datetime.DateTime()

	treatment = graphene.Field(TreatmentType)

	def mutate(self, info, title, content=None, date_created=None):
		treatment = Treatment.objects.get(pk=id)
		treatment.title = title if title is not None else treatment.title
		treatment.slug = slug if slug is not None else treatment.slug
		treatment.content = content if content is not None else treatment.content
		treatment.created_at = created_at if created_at is not None else treatment.created_at
		treatment.updated_at = updated_at if updated_at is not None else treatment.updated_at
		treatment.save()

class DeleteTreatment(graphene.Mutation):
	class Arguments:
		id = graphene.ID()
	treatment = graphene.Field(TreatmentType)

	def mutate(self, info, id):
		treatment = Treatment.objects.get(pk=id)
		if treatment is not None:
			treatment.delete()
		return DeleteProduct(treatment=treatment)

class Mutation(graphene.ObjectType):
  create_treatment = CreateTreatment.Field()
  update_treatment = UpdateTreatment.Field()
  delete_treatment = DeleteTreatment.Field()

schema = graphene.Schema(query=Query, mutation=Mutation)