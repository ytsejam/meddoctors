from django.urls import path, include

from rest_framework.routers import DefaultRouter

from .views import TreatmentsViewSet

router = DefaultRouter()
router.register("treatments", TreatmentsViewSet, basename="treatments")

urlpatterns = [
	path('', include(router.urls))
]