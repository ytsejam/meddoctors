from django.shortcuts import render
from django.core.exceptions import PermissionDenied
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from .serializers import TreatmentSerializer, TreatmentsubsSerializer
from .models import Treatment, Treatmentsubs
from medtourism.apps.categories.models import Category
from medtourism.apps.categories.serializers import CategorySerializer 

class TreatmentsViewSet(viewsets.ModelViewSet):
	serializer_class = TreatmentSerializer
	queryset = Treatment.objects.all()

	def get_queryset(self):
		return self.queryset.all()

	def perform_create(self, serializer):
		serializer.save()

	def perform_update(self, serializer):
		obj = self.get_object()

		if self.request.user != obj.created_by:
			raise PermissionDenied('Wrong object owner')

		serializer.save()

@api_view(['GET'])
@authentication_classes([])
@permission_classes([])
def get_categories(request):
	categories = Category.objects.all()
	serializer = CategorySerializer(categories, many=True)
	return Response(serializer.data)


@api_view(['GET'])
@authentication_classes([])
@permission_classes([])
def get_treatments(request):
	category_id = request.GET.get('category_id', '')
	treatments = Treatment.objects.all()
	if category_id:
		treatments = treatments.filter(categories__in=[int(category_id)])
	serializer = Treatmenterializer(treatments, many=True)
	return Response(serializer.data)

@api_view(['GET'])
@authentication_classes([])
@permission_classes([])
def get_treatment(request, slug):
	treatment = Treatment.objects.get(slug=slug)
	treatment_serializer = TreatmentSerializer(treatment)
	treatmentsubs_serializer = TreatmentsubsListSerializer(treatment.titles.all(), many=True)
	treatment_data = treatment_serializer.data
	data = {
		'treatment': treatment_data,
		'treatmentsubs': treatmentsubs_serializer.data
	}
	print(data)
	return Response(data)

 