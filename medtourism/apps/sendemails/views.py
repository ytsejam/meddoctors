from django.core.mail import send_mail, BadHeaderError
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from .forms import ContactForm
from django.http import JsonResponse
import json
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.template.loader import get_template
from django.core import mail
def contactView(request):
    if request.method=='POST':
        try:
            connection = mail.get_connection()
            connection.open()
            body = json.loads(request.body)
            print(body)
            subject = body['subject'] 
            name = body['name']
            contact_email = body['email']
            from_email = 'info@medtourism.uk'
            message = body['message']
            phone = body['phone']
            print(subject)
            print(from_email)
            html_content = render_to_string("email_template.html", 
                            {'title': 'test_email','subject': subject, 'message':message, 'contact_email': contact_email, 'phone': phone})
            text_content = strip_tags(html_content)
            msg =  mail.EmailMultiAlternatives('Contact Email', text_content, from_email, ['burak.akin@gmail.com', 'info@medtourism.uk'])
            msg.attach_alternative(html_content, "text/html")
            #msg.content_subtype="html"
            print(message)
            html_content_reply = render_to_string("email_template_reply.html", 
                            {'title': '[No Reply]', 'message':message, 'contact_email': contact_email})
            text_content_reply = strip_tags(html_content_reply)
            msg_reply =  mail.EmailMultiAlternatives('[No Reply] Your Mail has arrived', text_content, from_email, [contact_email])
            #msg_reply.content_subtype="html"
            msg_reply.attach_alternative(html_content_reply, "text/html")
            connection.send_messages([msg, msg_reply])
            connection.close()

            #send_mail(subject, message, from_email, [from_email, 'info@medtourism.uk'], fail_silently=False,)
            return HttpResponse(json.dumps({'response': 'Ok'}), content_type = "application/json")
        except:
            return HttpResponse(json.dumps({'response': 'Ko', 'message': 'Cannot be sent'}), content_type = "application/json")
    else:
        return HttpResponse(json.dumps({'response': 'Ko', 'message': 'Cannot be sent'}), content_type = "application/json")