from rest_framework import serializers
from .models import Tag

class TagSerializer(serializers.ModelSerializer):
	class Meta:
		model = Tag
		read_only_field = (
			"created_at",
			"updated_at",
		)
		fields = (
			"id",
			"title",
			"slug",
 		)