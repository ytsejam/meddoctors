from django.shortcuts import render
from rest_framework import viewsets
from .serializers import TagSerializer
from .models import Tag

from django.core.exceptions import PermissionDenied

class TagsViewSet(viewsets.ModelViewSet):
	serializer_class = TagSerializer
	queryset = Tag.objects.all()

	def get_queryset(self):
		return self.queryset.all()

	def perform_create(self, serializer):
		serializer.save()

	def perform_update(self, serializer):
		obj = self.get_object()

		if self.request.user != obj.created_by:
			raise PermissionDenied('Wrong object owner')

		serializer.save()