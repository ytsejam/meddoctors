from django import forms
from .models import Tag


class TagModelForm(forms.ModelForm):

    class Meta:
       model=Tag
       fields = [ "title", "slug"]
