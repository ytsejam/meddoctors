const path = require('path');
const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');

module.exports = merge(common, {
 mode: 'development',
 devtool: 'eval-cheap-module-source-map',
 devServer: {
   headers: {
      "Access-Control-Allow-Origin":"\*"
    },
    static: {
      directory: path.join(__dirname, 'assets'),
    },
    hot: true,
    client: {
      overlay: true,
    },
 },
});