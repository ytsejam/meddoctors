# -*- coding: utf-8 -*-
from .base import *
DEBUG = False
ALLOWED_HOSTS = ['medtourism.uk', 'www.medtourism.uk']
def get_env_variable(var_name):
    try:
        return os.environ[var_name]
    except KeyError:
        error_msg = "Set the %s environment variable" % var_name
        raise ImproperlyConfigured(error_msg)
 
DB_NAME = get_env_variable('DB_NAME')
DB_USER = get_env_variable('DB_USER')
DB_PASSWORD = get_env_variable('DB_PASSWORD')
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': DB_NAME,
        'USER': DB_USER,
        'PASSWORD': DB_PASSWORD,
        'HOST': 'localhost',
        'PORT': '',
    }
}
DJANGO_VITE_DEV_MODE=DEBUG
DJANGO_VITE_ASSETS_PATH = BASE_DIR / "bulma-vite" / "dist"

MEDIA_ROOT = BASE_DIR / 'media'
#STATIC_ROOT = '/home/ytsejam/public_html/medtourism/medtourism/static_cdn'
WEBPACK_LOADER = {
    'DEFAULT': {
        'CACHE': not DEBUG,
        'BUNDLE_DIR_NAME': 'dist/',  # must end with slash
        'STATS_FILE': '/home/ytsejam/public_html/medtourism/medtourism/frontend/webpack-stats.json',
        'POLL_INTERVAL': 0.1,
        'TIMEOUT': None,
        'IGNORE': [r'.+\.hot-update.js', r'.+\.map'],
        'LOADER_CLASS': 'webpack_loader.loader.WebpackLoader',
    }
}
#CSRF_COOKIE_NAME = "XSRF-TOKEN"
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
#SECURE_SSL_REDIRECT = True
#SESSION_COOKIE_SECURE = True
#SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
#SESSION_COOKIE_SECURE = True
#CSRF_COOKIE_SECURE = True

# Security Headers
SECURE_CONTENT_TYPE_NOSNIFF = True
SECURE_HSTS_INCLUDE_SUBDOMAINS = True
SECURE_HSTS_PRELOAD = True
SECURE_HSTS_SECONDS = 3600
DJANGO_VITE_ASSETS_PATH = BASE_DIR / "bulma-vite" / "dist"
WEBSITE_URL = 'https://medtourism.uk'
STATICFILES_DIRS = [
                    '/home/ytsejam/public_html/medtourism/medtourism/bulma-vite/dist']
STATIC_ROOT = '/home/ytsejam/public_html/medtourism/medtourism/static_cdn'


# uri to report policy violations
CSP_REPORT_URI = ["http://localhost:8000/fake-report-uri/"]

# default source as self
CSP_DEFAULT_SRC = ("'self'",)
CSP_INCLUDE_NONCE_IN = ["default-src"]
CSP_STYLE_SRC = ("'self'",
                 'fonts.googleapis.com',
                  "'unsafe-inline'",
                  "'unsafe-eval'",
                  "https")
# scripts from our domain and other domains
CSP_SCRIPT_SRC = ("'self'",
    "'unsafe-inline'",
    "'unsafe-eval'",
    "https",
    "http",
    "medtourism.uk/static",
    "https://medtourism.uk/static/@vite/client",
    "https://medtourism.uk/static/src/main",
    "medtourism.uk",
    'fonts.googleapis.com',
    "ajax.cloudflare.com",
    "static.cloudflareinsights.com",
    "www.google-analytics.com",
    "ssl.google-analytics.com",
    "cdn.ampproject.org",
    "www.googletagservices.com",
    "pagead2.googlesyndication.com")

# images from our domain and other domains
CSP_IMG_SRC = ("'self'",
    "www.google-analytics.com",
    "raw.githubusercontent.com",
    "googleads.g.doubleclick.net",
    "mdbootstrap.com")

# loading manifest, workers, frames, etc
CSP_FONT_SRC = ("'self'", 'fonts.gstatic.com', 'fonts.googleapis.com')
CSP_CONNECT_SRC = ("'self'",
    "'unsafe-inline'",
    "'unsafe-eval'",
    "https",
    "www.google-analytics.com",
     )
CSP_OBJECT_SRC = ("'self'", )
CSP_BASE_URI = ("'self'", )
CSP_FRAME_ANCESTORS = ("'self'", )
CSP_FORM_ACTION = ("'self'", )
CSP_INCLUDE_NONCE_IN = ('script-src', )
CSP_MANIFEST_SRC = ("'self'", )
CSP_WORKER_SRC = ("'self'", )
CSP_MEDIA_SRC = ("'self'",
        "https",
        "medtourism.uk", 
        "https://medtourism.uk")
