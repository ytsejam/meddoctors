# -*- coding: utf-8 -*-
from .base import *
CORS_ALLOWED_ORIGINS = [

    'http://localhost:3000',
    'http://127.0.0.1:3000',
    'http://localhost:8000',
    'http://127.0.0.1:8000',
    'http://127.0.0.1',

]
CORS_ORIGIN_ALLOW_ALL = True
CORS_ORIGIN_WHITELIST = [
    'http://localhost:8080',
    'http://localhost:3000',
    'http://127.0.0.1:8000',
    'http://127.0.0.1:8000/static',
    'http://127.0.0.1:3000',

]
CSRF_TRUSTED_ORIGINS = [
    "http://localhost:3000",
    "http://127.0.0.1:8000",
    "http://127.0.0.1",
    "https://googleads.g.doubleclick.net/pagead/id."
]

# uri to report policy violations
CSP_REPORT_URI = ["http://localhost:8000/fake-report-uri/"]

# default source as self
CSP_DEFAULT_SRC = ("'self'",)
CSP_INCLUDE_NONCE_IN = ["default-src"]
CSP_STYLE_SRC = ("'self'",
                 'fonts.googleapis.com',
                  "'unsafe-inline'",
                  "'unsafe-eval'",
                  "https")
# scripts from our domain and other domains
CSP_SCRIPT_SRC = ("'self'",
    "'unsafe-inline'",
    "'unsafe-eval'",
    "https",
    "http",
    "medtourism.uk/static",
    "https://medtourism.uk/static/@vite/client",
    "https://medtourism.uk/static/src/main",
    "medtourism.uk",
    'fonts.googleapis.com',
    "ajax.cloudflare.com",
    "static.cloudflareinsights.com",
    "www.google-analytics.com",
    "ssl.google-analytics.com",
    "cdn.ampproject.org",
    "www.googletagservices.com",
    "pagead2.googlesyndication.com")

# images from our domain and other domains
CSP_IMG_SRC = ("'self'",
    "www.google-analytics.com",
    "raw.githubusercontent.com",
    "googleads.g.doubleclick.net",
    "mdbootstrap.com")

# loading manifest, workers, frames, etc
CSP_FONT_SRC = ("'self'", 'fonts.gstatic.com', 'fonts.googleapis.com')
CSP_CONNECT_SRC = ("'self'",
    "'unsafe-inline'",
    "'unsafe-eval'",
    "https",
    "www.google-analytics.com",
     )
CSP_OBJECT_SRC = ("'self'", )
CSP_BASE_URI = ("'self'", )
CSP_FRAME_ANCESTORS = ("'self'", )
CSP_FORM_ACTION = ("'self'", )
CSP_INCLUDE_NONCE_IN = ('script-src', )
CSP_MANIFEST_SRC = ("'self'", )
CSP_WORKER_SRC = ("'self'", )
CSP_MEDIA_SRC = ("'self'",
        "https",
        "medtourism.uk", 
        "https://medtourism.uk")
