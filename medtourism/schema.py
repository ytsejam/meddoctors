import graphene
from medtourism.apps.pages.schema import schema as pagesschema
from medtourism.apps.treatments.schema import schema as treatmentsschema

class Query(
        pagesschema.Query,
        treatmentsschema.Query, 
        graphene.ObjectType,
        ):
    pass

class Mutation(
        pagesschema.Mutation, 
        treatmentsschema.Mutation, 
        graphene.ObjectType,):
    pass
schema = graphene.Schema(query=Query)