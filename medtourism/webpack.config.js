const path = require('path')
const { VueLoaderPlugin } = require('vue-loader')
const BundleTracker = require('webpack-bundle-tracker');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = (env = {}) => {
  return {

    mode:  'production' ,
    devtool:'source-map',
    entry: path.resolve(__dirname, './src/main.ts'),
    output: {
      path: path.resolve(__dirname, './dist'),
      filename: 'main.js',
      clean: true
    },
    
    plugins: [
      new VueLoaderPlugin(),
      new BundleTracker({
        filename: './webpack-stats.json',
        publicPath: 'http://0.0.0.0:8080/'
      }),
       new HtmlWebpackPlugin({
        title: 'Production',
      }),
    ]
    
  };
}
