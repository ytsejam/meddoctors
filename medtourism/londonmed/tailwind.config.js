module.exports = {
  content: [    
    "./index.html",    
    "./src/**/*.{vue,js,ts,jsx,tsx}",  
  ],
  colors: {
      'sse': '#00d1b2',
      'blue': '#1fb6ff',
      'purple': '#7e5bef',
      'pink': '#ff49db',
      'orange': '#ff7849',
      'green': '#13ce66',
      'yellow': '#ffc82c',
      'gray-dark': '#273444',
      'gray': '#8492a6',
      'gray-light': '#d3dce6',
    },
    fontFamily: {
      sans: ['Inter', 'sans-serif'],
      serif: ['Inter', 'serif'],
      
  },
  theme: {
    extend: {},
  },
  plugins: [
    require('@themesberg/flowbite/plugin'),
    require('@tailwindcss/aspect-ratio'),
  ],
}
