import { createApp } from 'vue'
import App from './App.vue'
import './index.css'
import axios from 'axios'
import VueAxios from 'vue-axios'
import router from './router/index.ts';
import '@themesberg/flowbite'
 
import * as apolloProvider from './apollo-client.ts'
createApp(App).use(router).use(apolloProvider.provider).use(VueAxios.axios).mount('#app')
