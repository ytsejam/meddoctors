import { createWebHistory, createRouter } from "vue-router";
import Home from "./../views/Home.vue";
import About from "./../views/About.vue";
import Page from "./../views/Page.vue";
import Pricing from "./../views/Pricing.vue";
import Consultation from "./../views/Consultation.vue";
import Contact from "./../views/Contact.vue";
import Treatments from "./../views/Treatments.vue";
import Treatment from "./../views/Treatment.vue";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/about",
    name: "About",
    component: About,
  },
  {
    path: "/page/:slug",
    name: "Page",
    component: Page,
    props:true
  },
  {
    path: "/pricing",
    name: "Pricing",
    component: Pricing,
    props:true
  },
  {
    path: "/online-consultation",
    name: "Consultation",
    component: Consultation,
  },
  {
    path: "/contact",
    name: "Contact",
    component: Contact,
  },
  {
    path: "/treatments",
    name: "Treatments",
    component: Treatments,
  },
  {
    path: "/treatment/:slug",
    name: "Treatment",
    component: Treatment,
    props: true,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;