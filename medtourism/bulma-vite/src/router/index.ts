import { createWebHistory, createRouter } from "vue-router";
import Home from "./../views/Home.vue";
import Contact from "./../views/Contact.vue";
import Page from "./../views/Page.vue";
 

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/contact",
    name: "Contact",
    component: Contact,
  },
  {
    path: "/pages/:slug",
    name: "Page",
    component: Page,
  },
];
 


const router = createRouter({
  history: createWebHistory(),
  routes,
});
 

export default router;