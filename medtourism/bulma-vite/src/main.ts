import { createApp } from 'vue'
import App from './App.vue'
import './../node_modules/bulma/css/bulma.css'
//import 'bulma-pro/css/bulma.css'
//import 'bulma-carousel/dist/css/bulma-carousel.min.css';
//import bulmaCarousel from 'bulma-carousel/dist/js/bulma-carousel.min.js';
//import 'vue3-carousel/dist/carousel.css';
//import { Carousel, Slide, Pagination, Navigation } from 'vue3-carousel';
import router from './router'
import store from './store'
import axios from 'redaxios'

axios.defaults.baseURL = 'https://medtourism.uk'
createApp(App).use(router, axios).use(store).mount('#app')
