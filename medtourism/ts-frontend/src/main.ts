import { createApp } from 'vue'
import App from './App.vue'
import './index.css';
import './assets/css/custom.css'
import './assets/css/style.css'
import router from './router/index.ts';
import * as apolloProvider from './apollo-client.ts' 
createApp(App).use(router).use(apolloProvider.provider).mount('#app')

