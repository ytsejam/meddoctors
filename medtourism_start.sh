#!/bin/bash
NAME="medtourism"    			                        #Name of the application (*)
DJANGODIR=/home/ytsejam/public_html/medtourism		        # Django project directory (*)
SOCKFILE=/home/ytsejam/public_html/medtourism/run/gunicorn.sock    # we will communicate using this unix socket (*)
USER=ytsejam            	                            	# the user to run as (*)
GROUP=http		                                     	# the group to run as (*)
NUM_WORKERS=1                                     		# how many worker processes should Gunicorn spawn (*)
DJANGO_SETTINGS_MODULE=medtourism.settings.production           # which settings file should Django use (*)
DJANGO_WSGI_MODULE=medtourism.wsgi                     		# WSGI module name (*)

echo "Starting $NAME as `whoami`"

# Activate the virtual environment
cd $DJANGODIR
source /usr/bin/virtualenvwrapper.sh
source /home/ytsejam/.virtualenvs/medtourism/bin/postactivate
workon medtourism

export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

# Create the run directory if it doesn't exist
RUNDIR=$(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR

# Start your Django Unicorn
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
exec /home/ytsejam/.virtualenvs/medtourism/bin/gunicorn ${DJANGO_WSGI_MODULE}:application \
  --name $NAME \
  --workers $NUM_WORKERS \
  --user $USER \
  --bind=unix:$SOCKFILE
